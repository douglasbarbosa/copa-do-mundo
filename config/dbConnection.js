const mysql = require('mysql');
let varsSis = require('../config/variaveisSistema.js');

let db_config = {
	host : 'localhost',
	user : varsSis.bd.usuario,
	password : varsSis.bd.senha,
	database : varsSis.bd.banco,
	connectionLimit : 50
};

let pool = mysql.createPool(db_config);

module.exports = function () {
	return pool;
}
