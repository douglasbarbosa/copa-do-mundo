var appFunction = function() {
  var express = require("express"),
    consign = require("consign"),
    bodyParser = require("body-parser"),
    expressValidator = require('express-validator'),
    expressSession = require('express-session');

  var app = express();
  app.set("trust proxy", true);
  app.use(bodyParser.json({
    limit: '50mb'
  }));
  app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true
  }));
  app.use(expressValidator());
  app.use(expressSession({
    secret: 'apiCopa',
    resave: false,
    saveUninitialized: false
  }));
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, ClienteApi");
    next();
  });

  consign()
    .include('app/routes')
    .then('app/models')
    .then('app/controllers')
    .into(app);

  consign()
    .then('config/variaveisSistema.js')
    .into(app);

  return app;
}

module.exports = function() {
  return appFunction();
};
