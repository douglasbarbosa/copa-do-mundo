var dbConnection = require('../../config/dbConnection.js');
const mongoDB = require('mongodb');
const mongoClient = mongoDB.MongoClient;
const urlMongo = 'mongodb://localhost:27017';

function Copa(){}

module.exports = function() {}

Copa.prototype.listarArbitros = function(){
  var pool = dbConnection();
  return new Promise(function(resolve, reject){
    pool.query(
      'CALL listar_arbitros()',
      [],
      function(erro, result){
        if(erro) console.log(erro);
        resolve(result[0]);
      }
    );
  });
}

Copa.prototype.listarEstadios = function(){
  var pool = dbConnection();
  return new Promise(function(resolve, reject){
    pool.query(
      'CALL listar_estadios()',
      [],
      function(erro, result){
        if(erro) console.log(erro);
        resolve(result[0]);
      }
    );
  });
}

Copa.prototype.listarSelecoes = function(idSelecao){
	var pool = dbConnection();
	return new Promise(function(resolve, reject){
		pool.query(
			'CALL listar_selecoes(?)',
			[
				idSelecao
			],
			function(erro, result){
				if(erro) console.log(erro);
				resolve(result[0]);
			}
		);
	});
}

Copa.prototype.inserirResultadosJogos = function(idJogo, golsMandante, golsVisitante){
	var pool = dbConnection();
	return new Promise(function(resolve, reject){
		pool.query(
			'CALL inserir_resultados_jogos(?, ?, ?)',
			[
				idJogo, 
				golsMandante, 
				golsVisitante
			],
			function(erro, result){
				if(erro) console.log(erro);
				resolve(result[0]);
			}
		);
	});
}

Copa.prototype.listarFases = function(idFase){
	var pool = dbConnection();
	return new Promise(function(resolve, reject){
		pool.query(
			'CALL listar_fases(?)',
			[
				idFase
			],
			function(erro, result){
				if(erro) console.log(erro);
				resolve(result[0]);
			}
		);
	});
}

Copa.prototype.listarGrupos = function(idGrupo){
	var pool = dbConnection();
	return new Promise(function(resolve, reject){
		pool.query(
			'CALL listar_grupos(?)',
			[
				idGrupo
			],
			function(erro, result){
				if(erro) console.log(erro);
				resolve(result[0]);
			}
		);
	});
}

Copa.prototype.listarGruposSelecoes = function(){
	var pool = dbConnection();
	return new Promise(function(resolve, reject){
		pool.query(
			'CALL listar_grupos_selecoes()',
			[],
			function(erro, result){
				if(erro) console.log(erro);
				resolve(result[0]);
			}
		);
	});
}

Copa.prototype.criarColecao = function(nomeColecao){
	return new Promise((resolve, reject) => {
		mongoClient.connect(urlMongo, (err, db) => {
			if (err) console.log(err);
			let dbo = db.db("mydb");
		  dbo.createCollection(nomeColecao, function(err, res) {
		    if (this.err) console.log(this.err);
		    	console.log(res);
			    db.close();
			});
		});
	});
}

Copa.prototype.inserirColecao = function(nomeColecao, obj){
	return new Promise((resolve, reject) => {
		mongoClient.connect(urlMongo, (err, db) => {
			if (err) console.log(err);
			let dbo = db.db("mydb");
	    dbo.collection(nomeColecao).insertOne(obj, function(err, res) {
		    if (this.err) console.log(this.err);
		    console.log(res);
		    db.close();
		  });
		});	
	});
}

Copa.prototype.listarColecao = function(nomeColecao){
	return new Promise((resolve, reject) => {
		mongoClient.connect(urlMongo, (err, db) => {
			if (err) console.log(err);
			let dbo = db.db("mydb");
			let query = {};
	    dbo.collection(nomeColecao).find(query).toArray(function(err, res) {
		    if (this.err) console.log(this.err);
		    resolve(res);
		    db.close();
		  }); 
		});	
	});
}


module.exports = function(){
	return Copa;
}