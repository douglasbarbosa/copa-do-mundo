module.exports = function(application){
	application.get('/arbitros/listar', function(req, res){
		application.app.controllers.sistema.autenticarClienteApi(
			res, req.headers.clienteapi, function(){
				application.app.controllers.copa.listagemDeAbitros(application, req, res);
			}
		);		
	});
	application.get('/estadios/listar', function(req, res){
		application.app.controllers.sistema.autenticarClienteApi(
			res, req.headers.clienteapi, function(){
				application.app.controllers.copa.listagemDeEstadios(application, req, res);
			}
		);		
	});
	application.post('/selecoes/listar', function(req, res){
		application.app.controllers.sistema.autenticarClienteApi(
			res, req.headers.clienteapi, function(){
				application.app.controllers.copa.listagemDeSelecoes(application, req, res);
			}
		);		
	});
	application.post('/resultado/jogos/inserir', function(req, res){
		application.app.controllers.sistema.autenticarClienteApi(
			res, req.headers.clienteapi, function(){
				application.app.controllers.copa.insercaoResultadosJogos(application, req, res);
			}
		);		
	});
	application.get('/fases/listar', function(req, res){
		application.app.controllers.sistema.autenticarClienteApi(
			res, req.headers.clienteapi, function(){
				application.app.controllers.copa.listagemDeFases(application, req, res);
			}
		);		
	});
	application.get('/grupos/listar', function(req, res){
		application.app.controllers.sistema.autenticarClienteApi(
			res, req.headers.clienteapi, function(){
				application.app.controllers.copa.listagemDeGrupos(application, req, res);
			}
		);		
	});
	application.get('/grupos/selecoes/listar', function(req, res){
		application.app.controllers.sistema.autenticarClienteApi(
			res, req.headers.clienteapi, function(){
				application.app.controllers.copa.listarGruposSelecoes(application, req, res);
			}
		);		
	});
	application.get('/listar/filmes/:lang', function(req, res){
		application.app.controllers.copa.listarFilmesPopulares(application, req, res);
	});
	application.get('/listar/collection/db', function(req, res){
		application.app.controllers.copa.listarColecaoDB(application, req, res);
	});
	application.get('/criar/collection/db', function(req, res){
		application.app.controllers.copa.criarColecaoDB(application, req, res);
	});
	application.get('/inserir/collection/db', function(req, res){
		application.app.controllers.copa.inserirColecaoDB(application, req, res);
	});
}
