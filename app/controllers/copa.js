const requestUrl = require('request');

module.exports.listagemDeAbitros = function(application, req, res){
	let copaModel = new application.app.models.Copa();
	copaModel.listarArbitros()
	.then((arbitros) => {
		res.send({
			status: 200,
			msg: arbitros
		});
	})
	.catch((erro) => {
		res.send({
			status: 500,
			msg: erro
		});
	});
}

module.exports.listagemDeEstadios = function(application, req, res){
	let copaModel = new application.app.models.Copa();
	copaModel.listarEstadios()
	.then((estadios) => {
		res.send({
			status: 200,
			msg: estadios
		});
	})
	.catch((erro) => {
		res.send({
			status: 500,
			msg: erro
		});
	});
}

module.exports.listagemDeSelecoes = function(application, req, res){
	let idSelecao = req.body.id;
	let copaModel = new application.app.models.Copa();
	copaModel.listarSelecoes(idSelecao)
	.then((selecoes) => {
		res.send({
			status: 200,
			msg: selecoes
		});
	})
	.catch((erro) => {
		res.send({
			status: 500,
			msg: erro
		});
	});
}

module.exports.insercaoResultadosJogos = function(application, req, res){
	let idJogo = req.body.idJogo;
	let golsMandante = req.body.golsCasa;
	let golsVisitante = req.body.golsFora;

	req.assert('idJogo', 'O ID do Jogo é imprescindível para completar a Operação').notEmpty();
	req.assert('golsCasa', 'Informe a Quantidade de Gols do Time Mandante para completar a Operação').notEmpty();
	req.assert('golsFora', 'Informe a Quantidade de Gols do Time Visitante para completar a Operação').notEmpty();

	let copaModel = new application.app.models.Copa();
	copaModel.inserirResultadosJogos(idJogo, golsMandante, golsVisitante)
	.then((dadosJogo) => {
		let empatado = false;
		let selecaoVitoria = '';

		dadosJogo[0].golsMandante == dadosJogo[0].golsVisitante ? empatado = true : empatado = false;

		dadosJogo[0].golsMandante > dadosJogo[0].golsVisitante ? selecaoVitoria = dadosJogo[0].nomeSelecaoMandante : selecaoVitoria = dadosJogo[0].nomeSelecaoVisitante;

		res.send({
			status: 200,
			msg: dadosJogo,
			vitoria: selecaoVitoria,
			empate: empatado
		});
	})
	.catch((erro) => {
		res.send({
			status: 500,
			msg: erro
		});
	});
}

module.exports.listagemDeFases = function(application, req, res){
	let idFase = req.params.id;
	let copaModel = new application.app.models.Copa();
	copaModel.listarFases(idFase)
	.then((fases) => {
		console.log(fases);
		res.send({
			status: 200,
			msg: fases
		});
	})
	.catch((erro) => {
		res.send({
			status: 500,
			msg: erro
		});
	});
}

module.exports.listagemDeGrupos = function(application, req, res){
	let idGrupo = req.params.id;
	let copaModel = new application.app.models.Copa();
	copaModel.listarGrupos(idGrupo)
	.then((grupos) => {
		res.send({
			status: 200,
			msg: grupos
		});
	})
	.catch((erro) => {
		res.send({
			status: 500,
			msg: erro
		});
	});
}

module.exports.listarGruposSelecoes = function(application, req, res){
	let copaModel = new application.app.models.Copa();
	copaModel.listarGruposSelecoes()
	.then((grupos) => {
		res.send({
			status: 200,
			msg: grupos
		});
	})
	.catch((erro) => {
		res.send({
			status: 500,
			msg: erro
		});
	});
}

module.exports.listarFilmesPopulares = function(application, req, res){
	let apiKey = 'd0e2e3b500ecc6e9e1ce8f2ae832baed';
	let urlAcess = 'https://api.themoviedb.org/3/movie/popular?api_key=';
	let language = '&language=';
	let siglaLang = req.params.lang;
	let objFilmes = [];

	requestUrl.get(urlAcess + apiKey + language + siglaLang, (err, data, body) => {
		if(err) throw err;
		let response = JSON.parse(body);
		response.results.forEach((filmes) => {
			objFilmes.push({
				nome_filme: filmes.title,
				descricao: filmes.overview,
				data_lancamento: filmes.release_date
			});
		});
		res.send({
			status: 500,
			dados: objFilmes
		});
	});
}

module.exports.listarColecaoDB = function(application, req, res){
	let copaModel = new application.app.models.Copa();
	copaModel.listarColecao('teste')
	.then((objResposta) => {
		res.send({
			status: 200,
			msg: objResposta
		});
	})
	.catch((erro) => {
		res.send({
			status: 500,
			msg: erro
		});
	});
}

module.exports.criarColecaoDB = function(application, req, res){
	let copaModel = new application.app.models.Copa();
	copaModel.criarColecao('teste')
	.then((res) => {
		// res.send({
		// 	status: 200,
		// 	msg: res
		// });
	})
	.catch((erro) => {
		res.send({
			status: 500,
			msg: erro
		});
	});
}

module.exports.inserirColecaoDB = function(application, req, res){

	let objColecao = {
		nome: "Douglas",
		texto: "Hello, World",
		metricas : {
			altura: 1.78,
			peso: "72kg",
			idade: {
				atual: 23,
				nascimento: "28/08/1995"
			}
		}
	};

	let copaModel = new application.app.models.Copa();
	copaModel.inserirColecao('teste', objColecao)
	.then((res) => {
		// res.send({
		// 	status: 200,
		// 	msg: res
		// });
	})
	.catch((erro) => {
		res.send({
			status: 500,
			msg: erro
		});
	});
}

